.TH MPWGEN 1 LOCAL
.SH NAME
mpwgen - Mike's Password Generator
.SH SYNOPSIS
.ds PN "mpwgen
\fI\*(PN\fP \fIMODE\fP [\fBoptions\fP]
.br
\fI\*(PN\fP \fB\-V\fP
.br
\fI\*(PN\fP \fB\-h\fP
.SH DESCRIPTION
\fIMpwgen\fP generates pseudo-random passwords in a variety of flavors.
.SS Phrase
Using a words file (by default \f(CRWORDFILE\fP) that has a newline
serpated list of words, generate a random phrase. The default length is 32
characters, although different lengths may be specified.  Optionally the
phrases can be obfuscated with 5 levels (~20% each) of l33tspeak-style.
In addition multicharacter l33tspeak replacements may be done.
.SS Pseudo-Word
Using basic understanding of phonetic structure, this will generate
pronounceable non-words of by default 8 characters.  Alternate length
may be specified. Optionally the pseudo-words can be obfuscated with
5 levels (~20% each) of l33tspeak-style.  In addition multicharacter
l33tspeak replacements may be done.
.SS Random
Simple random passwords may be created as well.  Length and character sets may be specified

.SH OPTIONS

.SS Phrase Mode
\fI\*(PN\fP phrase [-h] [-f FILE] [-L] [-$] [-l LENGTH]
.TP 5
-h, --help
show help message and exit
.TP 5
-f FILE, --word-file FILE
word file (default=/usr/share/dict/words)
.TP 5
-L, --l33t
pass through a l33tspeak filter
.TP 5
-$, --super-l33t
Allow multicharacter l33tspeak translations (breaks length)
.TP 5
-l LENGTH, --length LENGTH
Length in characters (default=32)

.SS Pseudo-Word Mode
\fI\*(PN\fP word [-h] [-L] [-$] [-l LENGTH]
.TP 5
-h, --help
show help message and exit
.TP 5
-L, --l33t
pass through a l33tspeak filter
.TP 5
-$, --super-l33t
Allow multicharacter l33tspeak translations (breaks length)
.TP 5
-l LENGTH, --length LENGTH
Length in characters (default=32)

.SS Random Mode
\fI\*(PN\fP random [-h] [-A] [-@] [-1] [-l LENGTH]
.TP 5
-h, --help
show help message and exit
.TP 5
-A, --capital-letters
include caps in the character set
.TP 5
-1, --numerals
include numerals in the character set
.TP 5
-@, --specials
include special characters in the character set
.TP 5
-l LENGTH, --length LENGTH
Length in characters (default=8)


.SH EXAMPLES
.TP
.nf
\fI\*(PN\fP \-n \-m "my message"
.fi
Show what would have occurred if you ran the tool.  Output shows commands that
would have been run, and the proposed text of the adjusted zones.
.TP
.nf
\fI\*(PN\fP \-m "my message"
.fi
Actually perform the work of updating the serials and reloading the zones.
.SH BUGS
\fI\*(PN\fP assumes all serial numbers will be on a line by themselves
followed by a comment with the word \fISerial\fP.  This is dumb,
but functional given convention.
.SH FILES
.TP
.nf
\f(CRWORDFILE\fP
.SH AUTHOR
Michael Messmore <mike@messmore.org>
