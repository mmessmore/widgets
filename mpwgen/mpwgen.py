#!/usr/bin/env python

import random
import argparse

# Simple script to generate random pronounceable passwords

consonants = ['b','c','d','f','g','h','j','k','l','m','n','p','r','s','t','v','w','x','z']
consonant_digraphs = ['ch', 'th', 'gh', 'ng', 'ph', 'qu', 'rh', 'sc', 'sh', 'wh', 'wr']
vowels = ['a', 'e', 'i', 'o', 'u', 'i']
vowel_digraphs = ['ae', 'ou', 'ie', 'igh', 'oi', 'oo', 'ea', 'ee', 'ai', 'au', 'ay', 'ei', 'eu', 'oa', 'oy', 'qu']
lowers = vowels + consonants + ['q']
caps = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
specials = ['~', '`', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '+', '=', '{', '}', '[', ']', '|', '\\', ':', ';', '"', '<', '>', ',', '.', '/', '?', "'"]
numerals = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

class WordlistError(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)

def fake_word(length):
	output = random.choice(consonants)
	i=1
	while len(output) < length:
		if (i % 2 == 0):
			output += random.choice(consonants + consonant_digraphs)
		else:
			output += random.choice(vowels + vowel_digraphs)
		i += 1
	if len(output) == length:
		return output
	return fake_word(length)

def phrase(length, wordlist):
	output = ""
	for w in wordlist:
		if output == "":
			output = w
			continue
		newoutput = output + " " + w
		if len(newoutput) == length:
			return newoutput
		if len(newoutput) > (length - 2):
			continue
		output = newoutput
	raise WordlistError("Wordlist Exhausted: " + output)

def rand(length, dic):
	output = ""
	while len(output) < length:
		output += random.choice(dic)
	return output

def wordlist_from_file(wordfile):
	wordlist = []
	for line in  wordfile:
		line = line.rstrip()
		if "'" in line:
			continue
		wordlist.append(line)
	random.shuffle(wordlist)
	return wordlist


def leet(text="", leetness=1, multi=False):
	leetmap_multi = {
		'a' : ['a', 'A', '4', '/-\\', '@', '^', '/\\' ],
		'b' : ['b', 'B', '8', '3', ']3', '|3', '13'],
		'c' : ['c', 'C', '<', '('],
		'd' : ['d', 'D', '>', ')', '])', '|)'],
		'e' : ['e', 'E', '3'],
		'f' : ['f', 'F', '|=', '(=', 'ph'],
		'g' : ['g', 'G', '6', '&'],
		'h' : ['h', 'H', '#'],
		'i' : ['i', 'I', '!', '1', '|', ']['],
		'j' : ['j', 'J', ';', '_|'],
		'k' : ['k', 'K', ']<'],
		'l' : ['l', 'L', '|', '1', '|_'],
		'm' : ['m', 'M', '/\/\\', '|\/|', '^^'],
		'n' : ['n', 'N', '/\/', '|/|'],
		'o' : ['o', 'O', '0', '()'],
		'p' : ['p', 'P', '|D', '|>'],
		'q' : ['q', 'Q', '?', '0,', '(,)'],
		'r' : ['r', 'R', '|2'],
		's' : ['s', 'S', '5', '$'],
		't' : ['t', 'T', '7', '+'],
		'u' : ['u', 'U', '|_|', '\_/'],
		'v' : ['v', 'V', '\/' , '\\\\//'],
		'w' : ['w', 'W', 'VV', 'vv', '\^/'],
		'x' : ['x', 'X', '><', ')('],
		'y' : ['y', 'Y', '`/', '\|/'],
		'z' : ['z', 'Z', '2', '7_'] }

	leetmap_single = {
		'a' : ['a', 'A', '4', '@', '^'],
		'b' : ['b', 'B', '8', '3'],
		'c' : ['c', 'C', '<', '('],
		'd' : ['d', 'D', '>', ')'],
		'e' : ['e', 'E', '3'],
		'f' : ['f', 'F'],
		'g' : ['g', 'G', '6', '&'],
		'h' : ['h', 'H', '#'],
		'i' : ['i', 'I', '1', '|', '!'],
		'j' : ['j', 'J', ';'],
		'k' : ['k', 'K'],
		'l' : ['l', 'L', '|', '1'],
		'm' : ['m', 'M'],
		'n' : ['n', 'N'],
		'o' : ['o', 'O', '0'],
		'p' : ['p', 'P'],
		'q' : ['q', 'Q', '?'],
		'r' : ['r', 'R'],
		's' : ['s', 'S', '5', '$'],
		't' : ['t', 'T', '7', '+'],
		'u' : ['u', 'U'],
		'v' : ['v', 'V'],
		'w' : ['w', 'W'],
		'x' : ['x', 'X'],
		'y' : ['y', 'Y'],
		'z' : ['z', 'Z', '2'] }

	leetmap = leetmap_single
	if multi:
		leetmap = leetmap_multi
	output = ""

	for c in text:
		if random.randrange(1,5) <= leetness:
			if c in leetmap.keys():
				output += random.choice(leetmap[c])
				continue
		output += c
	return output

def parse_args():
	description="Mike's Password Generator"
	epilog="See -h for each method for applicable options"
	parser = argparse.ArgumentParser( prog="mpwgen",
			description=description,
			epilog=epilog)
	subparsers = parser.add_subparsers(help="method")


	parser_phrase = subparsers.add_parser("phrase",
			help="Generate a pass phrase from words")
	parser_phrase.set_defaults(func=go_phrase)
	parser_phrase.add_argument("-f", "--word-file",
			dest="wordfile",
			type=argparse.FileType('r'),
			default='WORDFILE',
			metavar="FILE",
			help="word file (default=WORDFILE)")
	parser_phrase.add_argument("-L", "--l33t",
			dest="leet",
			action="count",
			help="pass through a l33tspeak filter")
	parser_phrase.add_argument("-$", "--super-l33t",
			dest="multileet",
			action="store_true",
			help="Allow multicharacter l33tspeak translations (breaks length)")
	parser_phrase.add_argument("-l", "--length",
			dest="length",
			help="Length in characters (default=%(default)s)",
			type=int,
			default=32)

	parser_word = subparsers.add_parser("word",
			help="Generate a pseudo-word pronouncable password")
	parser_word.set_defaults(func=go_word)
	parser_word.add_argument("-L", "--l33t",
			dest="leet",
			action="count",
			help="pass through a l33tspeak filter")
	parser_word.add_argument("-$", "--super-l33tness",
			dest="multileet",
			action="store_true",
			help="Allow multicharacter l33tspeak translations (breaks length)")
	parser_word.add_argument("-l", "--length",
			dest="length",
			help="Length in characters (default=%(default)s)",
			type=int,
			default=8)

	parser_rand = subparsers.add_parser("random",
			help="Generate a random password")
	parser_rand.set_defaults(func=go_rand)
	parser_rand.add_argument("-A", "--capital-letters",
			dest="caps",
			help="include caps in the character set",
			action='store_true')
	parser_rand.add_argument("-1", "--numerals",
			dest="nums",
			help="include numerals in the character set",
			action='store_true')
	parser_rand.add_argument("-@", "--specials",
			dest="specials",
			help="include special characters in the character set",
			action='store_true')
	parser_rand.add_argument("-l", "--length",
			dest="length",
			help="Length in characters (default=%(default)s)",
			type=int,
			default=8)

	return parser.parse_args()

def go_word(args):
	output = fake_word(args.length)
	return output

def go_phrase(args):
	if not 'wordfile' in args:
		args.wordfile = open('WORDFILE', 'r')
	wordlist = wordlist_from_file(args.wordfile)
	output = phrase(args.length, wordlist)
	return output

def go_rand(args):
	dic = lowers
	args.leet = 0
	if args.caps:
		dic = dic + caps
	if args.nums:
		dic = dic + numerals
	if args.specials:
		dic = dic + specials
	output = rand(args.length, dic)
	return output

def main():
	args = parse_args()
	output = args.func(args)
	if args.leet > 0:
		if args.leet > 5:
			args.leet == 5
		output = leet(output, args.leet, args.multileet)
	print output

if __name__ == '__main__':
	main()
