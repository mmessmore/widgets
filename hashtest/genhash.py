#!/usr/bin/env python

from ctypes import *
import sys
import os
import random
import argparse

def error(text):
	sys.stderr.write(text)

def get_def_alg_redhat():
	alg_map = {
		'descrypt' : '0',
		'bigcrypt' : '0',
		'md5' : '1',
		'sha256' : '5',
		'sha512' : '6'
	}
	authconfig = '/etc/sysconfig/authconfig'
	for line in  open(authconfig).readlines():
		if  line[0:15] == 'PASSWDALGORITHM':
			alg = line.partition('=')[2][0:-1]
	if alg in alg_map.keys():
		return alg_map[alg]
	return 0


def get_def_alg_bsd():
	alg_map = {
		'descrypt' : '0',
		'bigcrypt' : '0',
		'md5' : '1',
		'blowfish' : '2',
		'sha256' : '5',
		'sha512' : '6'
	}

	libutil = cdll.LoadLibrary('libutil.so')
	auth_getval = libutil.auth_getval
	auth_getval.restype = c_char_p
	
	alg = auth_getval("crypt_default")

	if alg == None:
		error("This shouls support login.conf capabilities.")
		error("But it doesn't.  Using the FreeBSD default of MD5")
		error("This is probably right.")
		alg = 'md5'

	if alg in alg_map.keys():
		return alg_map[alg]
	return 0

def get_def_alg():
	if os.path.exists('/etc/sysconfig/authconfig'):	
		return get_def_alg_redhat()
	if os.path.exists('/etc/auth.conf'):
		return get_def_alg_bsd()
	# fall back to DES
	return 0

def main():

	# Some basic character sets
	lower = "abcdefghijklmnopqrstuvwxyz"
	upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	num = "0123456789"
	spec = "./"

	def_alg = get_def_alg()

	version="genhash.py v0.9"
	version = version + "Default hash:%s" % def_alg;
	
	description="Generate Password Hashes using the system crypt() function"
	epilog= """
Known hash algorithms supported:
?	auto
		On FreeBSD or Linux, lookup system default type
0	DES
		This is our way of communicating not-a-value.  DES is supported
		everywhere and should be used nowhere.
1	MD5 
		Better than DES, not as good as anything else.  Supported
		on most platforms.  May have issues on older commercial Unices.
2[axy]	Blowfish
		*BSD only.  Support may be spotty.
		2  - original (obsolete)
		2a - possibly flawed, possibly fixed implementation
		2x - post-flaw notation for flawed hashes
		2y - "new" non-flawed implementation

		YMMV

3	NT-Hash
		*BSD only.  You may use this for Windows compatability
		or as a joke to pull on your friends.

5	SHA-256
		Betterer.  Recent Linux and BSD

6	SHA-512
		Best.  Recent Linux and BSD
		
""" 

	parser = argparse.ArgumentParser(
		description = description,
		version = version,
		epilog = epilog,
		formatter_class = argparse.RawTextHelpFormatter)

	parser.add_argument('-a', '--algorithm', 
				action = 'store',
				dest = 'algorithm',
				default = '?',
				help = 'Hash algorithm')
	parser.add_argument('-p', '--password', 
				action = 'store',
				dest = 'password',
				default = '',
				help = 'Password string')

	args = parser.parse_args()


	alg = str(args.algorithm)
	if alg == '?':
		alg = str(def_alg)
	
	if not alg in ['0', '1', '2', '2a', '2x','2y', '3', '5', '6']:
		error( "Error: Unsupported algorithm %s" % alg)
		parser.print_help()
		sys.exit(1)

	passwd = args.password
	
	if passwd == '':
		import getpass
		passwd = getpass.getpass()
		if passwd != getpass.getpass('Repeat password:'):
			error("Error: Passwords do not match")
			sys.exit(1)


	salties = lower + upper + num + spec
	randlen = 8
	randval = ''.join(random.sample(salties, randlen))
	salt = "$%s$%s$" % (alg, randval)

	if alg == '0':
		salt = randval[0:2]

	libcrypt = cdll.LoadLibrary('libcrypt.so')
	crypt = libcrypt.crypt
	crypt.restype = c_char_p

	print crypt(passwd, salt)
	sys.exit(0)

if __name__ == '__main__':
	main()
