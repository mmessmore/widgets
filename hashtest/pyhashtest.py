#!/usr/bin/python

from ctypes import *
import sys

def usage():
	print """ Usage: %s KEY SALT

 KEY                     password string to be hashed
 SALT                    salt value to use for hash

 This is a straight-up call to the system's crypt().
 See `man 3 crypt' for more info.
""" % (sys.argv[0])

def main():
	libcrypt = cdll.LoadLibrary('libcrypt.so')
	crypt = libcrypt.crypt
	crypt.restype = c_char_p

	if len(sys.argv) != 3:
		usage()
		sys.exit(1)

	print crypt(sys.argv[1], sys.argv[2])
	sys.exit(0)

if __name__ == '__main__':
	main()
