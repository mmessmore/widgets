#define _XOPEN_SOURCE
#include <unistd.h>
#include <stdio.h>

void usage(char *prog);

int main(int argc, char *argv[])
{

	/* Make sure we have enough arguments */
	if (argc != 3) {
		usage(argv[0]);
		return 1;
	}

	/* Pass them to crypt() */
	printf("%s\n", crypt(argv[1], argv[2]));

	return 0;
}

/* Print usage message */
void usage(char *prog)
{
	printf("Usage: %s KEY SALT\n\n", prog);
	printf(" KEY                     password string to be hashed\n");
	printf(" SALT                    salt value to use for hash\n\n");
	printf(" This is a straight-up call to the system's crypt().\n");
	printf(" See `man 3 crypt' for more info.\n");
}
