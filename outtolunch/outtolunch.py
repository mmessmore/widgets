#!/usr/bin/env python

import BaseHTTPServer
import SocketServer
import socket

import argparse
import sys
import signal
import re

port = 8080
retry = None
response = """
<html>
<head>
<title>Down For Maintenance</title>
<body>
<h1>Down For Maintenance</h1>
<p>We apologize for the inconvenience.</p>
</body>
</html>
"""

class MyRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
	def __init__(self, req, client_addr, server):
		BaseHTTPServer.BaseHTTPRequestHandler.__init__(self, req, client_addr, server)
	def do_GET(self):
		self.respond()

	def do_POST(self):
		self.respond()

	def do_HEAD(self):
		self.send_response(503)
		if not retry == None:
                        self.send_header("Retry-After", retry)
		self.end_headers()
		

	def respond(self):
		self.send_response(503)
		if not retry == None:
			self.send_header("Retry-After", retry)
		self.send_header("Content-type", "text/html")
		self.send_header("Content-length", len(response))
		self.end_headers()
		self.wfile.write(response)

def rfc1123_date(string):
	msg = "%s is not a properly formatted RFC1123 formatted date" % string
	pat = r'^(((Mon)|(Tue)|(Wed)|(Thu)|(Fri)|(Sat)|(Sun)), \d{2} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \d{4} (0\d|1\d|2[0-3])(\:)(0\d|1\d|2\d|3\d|4\d|5\d)(\:)(0\d|1\d|2\d|3\d|4\d|5\d) (GMT))$'
	if not re.match(pat, string):
		raise argparse.ArgumentTypeError(msg)
	return string

def parse_args():
	version = "outtolunch v%s" % "$Revision: 1.1 $".split()[1]
	description = "Respond to all HTTP GET & POST requests with a 503 and message"
	epilog = """
Example RFC1123 Date: 'Fri, 31 Dec 1999 23:59:59 GMT' (must be GMT)
"""

	parser = argparse.ArgumentParser(
		description=description,
		version=version,
		epilog=epilog,
		formatter_class=argparse.RawTextHelpFormatter)

	parser.add_argument('-r', '--response', type=argparse.FileType('r'),
				metavar='FILE',
				dest='response',
				help='File containing custom HTML response')

	parser.add_argument('-p', '--port', action='store',
				dest='port',
				type=int,
				help='Port to listen on (Default: 8080)')

	parser.add_argument('-d', '--retry-date', action='store',
				type=rfc1123_date,
				dest='date',
				help="RFC1123 date for end time of outage")

	return parser.parse_args()


def serve():
	httpd = SocketServer.TCPServer(("", port), MyRequestHandler)
	print "Now serving on port:", port
	print "Hit Ctrl-C to exit"
	try:
		httpd.serve_forever()
	except socket.error as e:
		print "Could not bind to socket."
		print "%d: %s" % (e.errno, e.strerror)
		sys.exit(e.errno)

def sigint_handler(signal, frame):
	print "\nCtrl+C! Exiting!"
	sys.exit(0)

	
if __name__ == "__main__":
	signal.signal(signal.SIGINT, sigint_handler)
	args = parse_args()
	if not args.response == None:
		response = args.response.read()
		print response
	if not args.port == None:
		port = args.port
	retry=args.date
	serve()
