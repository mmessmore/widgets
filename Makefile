# Makefile to install ALL THE THINGS!
#

SUBDIRS=autoproxy cursort dns hashtest mdcheck mps outtolunch freebsd_util

all:
	@echo
	@echo "You probably want to 'make install' to install everything"
	@echo "You can DEST=/foo to change the destination"
	@echo "By default it's ~/"
	@echo

install: $(SUBDIRS)
	for d in $(SUBDIRS); do $(MAKE) -C ./$$d install; done

deinstall: $(SUBDIRS)
	for d in $(SUBDIRS); do $(MAKE) -C ./$$d deinstall; done

clean: $(SUBDIRS)
	for d in $(SUBDIRS); do $(MAKE) -C ./$$d clean; done
