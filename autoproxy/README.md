# autoproxy

## Set proxy based on Wireless network

Not the cleanest solution, I guess, but this is what I use to make sure
my proxy is automagically set in all Gnome {2,3}, Cinnamon, and Mate,
depending on where I am.

This makes some assumptions:

1. You only care about setting based on wireless networks (SSIDs)
2. You only have one active wireless adapter.

These could be worked around.  I just haven't.

I use this on Linux Mint Debian Edition.  It should work on any
Debian-based distro.  

It might work on Ubunutu, I haven't looked.

It could work on Fedora/RedHat with a little work, it just needs to be 
fired up when the wireless connection has been established.

Improvements are welcome.
