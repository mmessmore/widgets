#!/bin/bash

program=${0##*/}
TMP=$(mktemp /tmp/${program}.XXXXXXXX) || exit 1
trap 'rm -f $TMP' EXIT TERM INT QUIT

usage() {
	cat <<EOD
${program} [-s] -H HOST -p PORT
${program} -c
${program} -h

-c       clear/unset proxy
-H       proxy hostname or IP address
-h       this help message
-p       proxy port
-s       set as SOCKS proxy (vs http)

requires either -h and -p set, or -c
EOD
}

error() {
	echo "$program: $1" >&2
}


set_gnome_proxy() {
	proxy=$1
	port=$2
	socks=$3
	pgrep gnome-session >/dev/null || return
	if [ $socks = true ]; then
		gsettings set org.gnome.system.proxy mode manual
		gsettings set org.gnome.system.proxy.socks host "$proxy"
		gsettings set org.gnome.system.proxy.socks port "$port"
		return 0
	fi
	gsettings set org.gnome.system.proxy.http enabled true
	gsettings set org.gnome.system.proxy mode manual
	gsettings set org.gnome.system.proxy.http host "$proxy"
	gsettings set org.gnome.system.proxy.http port "$port"
	gsettings set org.gnome.system.proxy.https host "$proxy"
	gsettings set org.gnome.system.proxy.https port "$port"
	gsettings set org.gnome.system.proxy use-same-proxy true
}

unset_gnome_proxy() {
	pgrep gnome-session >/dev/null || return
	gsettings set org.gnome.system.proxy.http enabled false
	gsettings set org.gnome.system.proxy mode none
}

set_mate_proxy() {
	proxy=$1
	port=$2
	socks=$3
	pgrep mate-session >/dev/null || return
	if [ $socks = true ]; then
		mateconftool-2 --type string --set '/system/proxy/mode' "manual"
		mateconftool-2 --type string --set '/system/socks_proxy/host' "$proxy"
		mateconftool-2 --type int --set '/system/socks_proxy/port' "$port"
	fi
	mateconftool-2 --type string --set '/system/proxy/mode' "manual"
	mateconftool-2 --type string --set '/system/http_proxy/host' "$proxy"
	mateconftool-2 --type int --set '/system/http_proxy/port' "$port"
	mateconftool-2 --type bool \
		--set '/system/http_proxy/use_same_proxy' "TRUE"
	mateconftool-2 --type bool \
		--set '/system/http_proxy/use_http_proxy' "TRUE"
}

unset_mate_proxy() {
	pgrep mate-session >/dev/null || return
	mateconftool-2 --type string --set '/system/proxy/mode' "none"
	mateconftool-2 --unset '/system/http_proxy/host'
	mateconftool-2 --unset '/system/http_proxy/port'
	mateconftool-2 --unset '/system/socks_proxy/host'
	mateconftool-2 --unset '/system/socks_proxy/port'
}

set_gnome2_proxy() {
	proxy=$1
	port=$2
	socks=$3
	pgrep gnome-session >/dev/null || return
	if [ $socks = true ]; then
		gconftool-2 --type string --set '/system/proxy/mode' "manual"
		gconftool-2 --type string --set '/system/socks_proxy/host' "$proxy"
		gconftool-2 --type int --set '/system/socks_proxy/port' "$port"
	fi
	gconftool-2 --type string --set '/system/proxy/mode' "manual"
	gconftool-2 --type string --set '/system/http_proxy/host' "$proxy"
	gconftool-2 --type int --set '/system/http_proxy/port' "$port"
	gconftool-2 --type bool \
		--set '/system/http_proxy/use_same_proxy' "TRUE"
	gconftool-2 --type bool \
		--set '/system/http_proxy/use_http_proxy' "TRUE"
}

unset_gnome2_proxy() {
	pgrep gnome-session >/dev/null || return
	gconftool-2 --type string --set '/system/proxy/mode' "none"
	gconftool-2 --unset '/system/http_proxy/host'
	gconftool-2 --unset '/system/http_proxy/port'
	gconftool-2 --unset '/system/socks_proxy/host'
	gconftool-2 --unset '/system/socks_proxy/port'
}

set_kde4_proxy(){
	proxy=$1
	port=$2
	socks=$3
	[ -f ${HOME}/.kde/share/config/kioslaverc ] &&
		rc=${HOME}/.kde/share/config/kioslaverc
	[ -f ${HOME}/.kde4/share/config/kioslaverc ] &&
		rc=${HOME}/.kde4/share/config/kioslaverc
	[ -z "$rc" ] && return
	(
		egrep -v '(^[a-z][a-z]*Proxy|^ProxyType|^NoProxyFor)' $rc
		echo "ProxyType=1"
		echo "NoProxyFor=localhost,127.0.0.0/8,10.0.0.0/8,192.168.0.0/16,172.16.0.0/12"
		if [ $socks = "true" ]; then
			echo "ftpProxy="
			echo "httpProxy="
			echo "httpsProxy="
			echo "socksProxy=$proxy $port"
		else
			echo "httpProxy=$proxy:$port/"
			echo "httpsProxy=$proxy:$port/"
		fi
	) >$TMP
	install $TMP $rc
	pgrep dbus >/dev/null &&
	dbus-send --type=signal /KIO/Scheduler org.kde.KIO.Scheduler.reparseSlaveConfiguration string:""
}

unset_kde4_proxy(){
	[ -f ${HOME}/.kde/share/config/kioslaverc ] &&
		rc=${HOME}/.kde/share/config/kioslaverc
	[ -f ${HOME}/.kde4/share/config/kioslaverc ] &&
		rc=${HOME}/.kde4/share/config/kioslaverc
	[ -z "$rc" ] && return
	(
		egrep -v '(^[a-z][a-z]*Proxy|^ProxyType|^NoProxyFor)' $rc
		echo "ProxyType=0"
	) >$TMP
	install $TMP $rc
	pgrep dbus >/dev/null &&
	dbus-send --type=signal /KIO/Scheduler org.kde.KIO.Scheduler.reparseSlaveConfiguration string:""
}


set_shell_proxy() {
	proxy=$1
	port=$2
	cat <<-EOF > ${HOME}/.proxy
        export http_proxy=http://$proxy:$port
        export https_proxy=http://$proxy:$port
        export ftp_proxy=$proxy:$port
	EOF
	chmod 755 ${HOME}/.proxy
}

unset_shell_proxy() {
	cat <<-EOF > ${HOME}/.proxy
        unset http_proxy
        unset https_proxy
        unset ftp_proxy
        unset socks_proxy
	EOF
	chmod 755 ${HOME}/.proxy
}

# defaults
SOCKS=false

# parse args
while getopts 'chH:p:s' opt
do
	case $opt in 
	c)
		ACTION="clear"
		;;
	h)
		usage
		exit 0
		;;
	H)
		HOST="$OPTARG"
		ACTION="set"
		;;
	p)
		PORT="$OPTARG"
		ACTION="set"
		;;
	s)
		SOCKS=true
		;;
	*)
		error "Unrecognized argument: $opt"
		usage
		exit 128
		;;
	esac
done

if [ -z "$ACTION" ] 
then
	error "No action specified."
	usage
	exit 128
fi

if [ $ACTION = "set" ] 
then
	if [ -z "$HOST" ] || [ -z "$PORT" ]
	then
		error "Host or Port not specified"
		usage
		exit 128
	fi
	set_gnome_proxy $HOST $PORT $SOCKS
	set_mate_proxy $HOST $PORT $SOCKS
	set_gnome2_proxy $HOST $PORT $SOCKS
	set_shell_proxy $HOST $PORT $SOCKS
	set_kde4_proxy $HOST $PORT $SOCKS

elif [ $ACTION = "clear" ]
then
	unset_gnome_proxy
	unset_mate_proxy
	unset_gnome2_proxy
	unset_shell_proxy
	unset_kde4_proxy
else
	echo "ACTION:$ACTION"
	error "Shouldn't be here... hmmm."
	exit 1
fi
