#!/bin/bash

pgrep X >/dev/null || exit

export DISPLAY=:0

getent passwd | egrep ':[0-9]{4,}:[0-9]{4,}:' | grep -v ^nobody | cut -d: -f1 |
while read user
do
	sudo -i -u $user BINDIR/autoproxy
done
