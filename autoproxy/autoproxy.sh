#!/bin/bash

[ -f ${HOME}/.autoproxy ] || exit

# Get The SSID
SSID=$( iwconfig 2>/dev/null |grep 'ESSID' | sed 's/[^"]*"\([^"]*\)".*/\1/')

# Try and set the proxy based on our rules
MSG=$(grep -v '^#' ${HOME}/.autoproxy |
while IFS="," read RSSID NAME HOST PORT
do
	if [ "$SSID" = "$RSSID" ]
	then
		if [ -z "$HOST" ] || [ -z "$PORT" ]
		then
			proxy -c >&2
			echo "$NAME: Clearing proxy"
		else
			proxy -H $HOST -p $PORT >&2
			echo "$NAME: Setting proxy to $HOST:$PORT"
		fi
	fi
done)

# Otherwise assume no proxy
if [ -z "$MSG" ]
then
	proxy -c fallthrough  >&2
	MSG="Unknown Network: Clearing proxy"
fi

# Let us know it happened
notify-send "$MSG"
