# mdcheck

Simple script to validate markdown rendering.  Requires the markdown package and a normal unix desktop.

It:

1. generates html
2. validates it with tidy
2. displays it in the default browser
