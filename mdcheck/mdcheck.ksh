#!/bin/ksh

program=${0##*/}
version="0.1"

usage() {
	cat <<EOM
$program FILE1 [FILE2 ... FILEn]
$program [-hV]

FILE      markdown formatted file

-h       this help message
-V       print version

EOM
}

version() {
	echo $program $version
}

error() {
	echo "${program}: $@" >&2
}

bail() {
	status=$1
	shift
	message=$@

	error $message
	exit $status
}

OPTS=$@

while getopts 'hV' OPT; do
	case OPT in
		h) usage && exit 0 ;;
		V) version && exit 0 ;;
		*) usage && bail 22 "Invalid option: $OPT";;
	esac
done

shift $(( OPTIND - 1))
[ $# -lt 1 ] && usage && bail 22 "No files specified"

for f in $@; do
	[ -f "$f" ] || bail 22 "Not a file: $f"
done

TMP=$(mktemp /tmp/mdcheck.tmp.XXXXXX) || bail $? "Could not create tempfile"
trap "rm -f $TMP" EXIT TERM INT

cat <<EOM >>$TMP
<!DOCTYPE html>
<html>
<head><title>Markdown Test for $OPTS</title></head>
<body>
EOM

markdown $OPTS >>$TMP

echo "</body></html>" >> $TMP

tidy -e $TMP

xdg-open file://$TMP
