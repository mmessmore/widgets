#!/usr/bin/env python

# A dumb configurable UDP listener that executes a process when told
# the correct message

import sys, os, socket, signal
import ConfigParser

def bail(error=0, msg=""):
    if msg != "":
        sys.stderr.write(sys.argv[0] + ": " + msg + "\n")
    sys.exit(error)

def execute(exe):
    path = exe.split(' ')[0]
    args = exe.split(' ')
    os.spawnv(os.P_WAIT, path, args)

def parse_conf():
    cfile = ""
    for cpath in [ '/etc/puntd.conf', '/usr/local/etc/puntd.conf',
            './puntd.conf',
            os.path.join(os.environ.get('HOME'), '.puntd.conf') ]:
        if os.path.exists(cpath):
            cfile = cpath
            break
    if cfile == "":
        sys.stderr.write(sys.argv[0] + ": no config file present\n")

    config = ConfigParser.SafeConfigParser({
        'addr' : '127.0.0.1',
        'port' : 3333,
        'msg'  : socket.gethostname(),
        'exe'  : "/usr/bin/puppet agent --onetime"})
    config.read(cfile)

    return(config.get('puntd', 'addr'),
           config.getint('puntd', 'port'),
           config.get('puntd', 'msg'),
           config.get('puntd', 'exe'))

def sig_handlers(exe):
    signal.signal(signal.SIGHUP, lambda x,y : execute(exe))
    signal.signal(signal.SIGUSR1, lambda x,y : execute(exe))
    signal.signal(signal.SIGINT, lambda x,y : bail(0, "Caught ^c. Exited"))
    signal.signal(signal.SIGTERM, lambda x,y : bail(1))

def worker(addr, port, msg, exe):
    sock = socket.socket(socket.AF_INET, # Internet
                         socket.SOCK_DGRAM) # UDP
    sock.bind((addr, port))

    while True:
        data, raddr = sock.recvfrom(1024) # buffer size is 1024 bytes
        print "received message:", data
        if data == msg:
            execute(exe)

def main():
    (addr, port, msg, exe) = parse_conf()
    sig_handlers(exe)
    worker(addr, port, msg, exe)

if __name__ == '__main__':
    main()
