#!/usr/bin/env python

import socket
import sys

UDP_IP = "127.0.0.1"
UDP_PORT = 3123
msg = "yes"

if len(sys.argv) > 1:
    msg = sys.argv[1]

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.sendto(msg, (UDP_IP, UDP_PORT))
