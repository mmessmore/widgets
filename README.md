# Various little widgets I've made and use

These don't deserve to be projects in their own right, but I like to
preserve them for revision control and backup.  Feel free to reuse
anything here in accordance to the BSD license specified in
[LICENSE](https://bitbucket.org/mmessmore/widgets/src/master/LICENSE).

If you find something useful, please let me know.  I'd appreciate it.


## autoproxy
This sets my proxy config based on SSID when I connect to a
network. Debian-ish specific.  May work on Ubuntu too.  Made to work
with command line and Gnome 2, Gnome 3, MATE, Cinnamon, and KDE.

[README](https://bitbucket.org/mmessmore/widgets/src/master/autoproxy/README.md)

## cursort
A toy I was making with curses that accepts streaming input and sorts
it on the fly as it goes.

##dns
Various dns scripts I use on a regular basis.  I put the non-site specific
ones here.

## freebsd_util
Handy scripts I use on FreeBSD.  That's all.

## hashtest
These are little gadgets I made to play with crypt() to experiment with
behavior across platforms for a Unix/Linux account management system I
help maintain.  Not all Unices have a convenient command to call crypt()
directly, so this includes C and Python versions, as well as a Python
password hash generating script that outlines Hash support on various
platforms.

## undown
Little python scripts using the Reddit API via
[PRAW](https://praw.readthedocs.org/en/latest/) to account for bad
behavior on Reddit.  Basically these are just good scripts to upvote
things that are getting automatic downvotes by evil bots.  They are
made to follow all the rules of Reddit, and are just made to be run from
cron(8) or some scheduler.

## mdcheck
A little script to validate markdown (like this) before I commit.  It just
processes it with markdown(1), checks it with tidy(1), and opens it in
your default browser to check out.
[README](https://bitbucket.org/mmessmore/widgets/src/master/mdcheck/README.md)

## mps
This is just a little verification script to check for non-system apps
on a host.  It looks for running processes, scheduled jobs, and listening
ip sockets.  I forgot why I named is mps.
[README](https://bitbucket.org/mmessmore/widgets/src/master/mps/README.md)

##outtolunch
A simple python webserver that just sends an outtolunch page.  Great as
a placeholder during a maintenance window or whatever.
