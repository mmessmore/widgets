#!/usr/bin/env python

#
# cursort - A live sorted display of lines of text
#
# Author: Mike Messmore mmessmore(at)gmail.com
#
# To be used like:
# $ df -sk /var/* | cursort -n
#
# This is mostly an excuse to play with curses to solve a problem I have
# with wanting to see sorted output, without having to wait on the left-hand
# side of the pipe to finish.
#

# Basic python/os stuff
import sys
import os
import getopt
import signal
import errno

# probably should try to remove these dependencies
import time
import re

# and of course, curses
import curses

def usage():
	print """
%(program)s usage:

%(program)s [-n]
%(program)s -h
%(program)s -V

-n          natural sort (numeric-ish)
-h          this help message
-V          version information

%(program)s reads from STDIN and prints the sorted output as it recieves it to
the screen using [n]curses.  

""" % { "program" : sys.argv[0] }

def version():
	print "cursort v0.1"
	sys.exit(0)

def error(msg):
	sys.stderr.write("%s: %s\n" % (sys.argv[0], msg))

def atoi(s):
	try:
		return int(s)
	except:
		return s

def numeric(text):
	return [ atoi(c) for c in re.split('(\d+)', text) ]

def exit(val=0):
	curses.echo()
	curses.endwin()
	print "Exiting: %d" % (val)
	sys.exit(val)

def e_handler(signum, frame):
	exit(2)

def parse_args():
	options = {}
	options['sort'] = None

	try:
		opts, args = getopt.getopt(sys.argv[1:], 'hnV')
	except getopt.GetoptError, err:
		usage()
		bail(errno.EINVAL, err) # invalid argument

	# now handle the options
	for o, a in opts:
		if o == '-h':
			usage()
			sys.exit(0)
		elif o == '-n':
			options['sort']=numeric
		elif o == '-V':
			version()
			sys.exit(0)
		else:
			bail(errno.EINVAL, "Unhandled option")

	return options

class Window:
	def __init__(self, input = sys.stdin, sort_key=None):
		self.input = input
		self.sort_key = sort_key
		self.offset = 0

		self.list = []

		stdscr = curses.initscr()
		(height, width) = stdscr.getmaxyx()
		curses.noecho()
		stdscr.nodelay(1)
		stdscr.idlok(1)
		stdscr.setscrreg(1,height-1)
		self.window = stdscr

	def stream(self):
		l = self.input.readline()
		while l != "":
			self.add_element(l.rstrip())
			self.input_check()
			l = self.input.readline()

	def idle(self):
		while True:
			self.input_check()
			time.sleep(1)

	def display(self):
		self.window.clear()
		self.window.addstr(0,0, "Cursort: Press Q to Exit", curses.A_UNDERLINE)
		(height, width) = self.window.getmaxyx()
		y = 2
		for i in self.list[self.offset:self.offset + height - 2]:
			self.window.addstr(y, 1, i, curses.A_NORMAL)
			y+=1
		self.window.refresh()

	def add_element(self, element):
		self.list.append(element)
		self.list.sort(key=self.sort_key)

	def input_check(self):
		ch = self.window.getch()
		(height, width) = self.window.getmaxyx()
		if ch == -1:
			return True
		elif ch == curses.KEY_UP:
			self.offset -= 1
		elif ch == curses.KEY_DOWN:
			self.offset +=1
		elif ch == curses.KEY_PPAGE:
			self.offset -= height
		elif ch == curses.KEY_NPAGE:
			self.offset += height
#		elif ch == ord('q'):
#			exit(9)
#		elif ch == ord('Q'):
#			exit(8)

		self.display()


def main():
	options = parse_args()

	window = Window(sort_key = options['sort'])

	# Gracefully handle stuff
	signal.signal(signal.SIGTERM, e_handler)
	signal.signal(signal.SIGINT, e_handler)

	window.stream()
	window.idle()



if __name__ == '__main__':
	main()
