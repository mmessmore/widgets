#!/usr/bin/env python

# Share The Love
# Simple script to be encouraging to an entire subreddit
import praw
import os
import sys

comment_limit = 200
sub_limit = 50

# This just keeps me from hardcoding creds
# Needs a file in ~/.stl with format
# user
# password
# subreddit you want to patrol (no /r/)
settings = open(os.path.join(os.environ['HOME'], '.stl'))
user = settings.readline().strip()
password = settings.readline().strip()
subreddit = settings.readline().strip()

# Note this should probably be purged after some length of time
comment_log = os.path.join(os.environ['HOME'], '.stl.log')

ids = []
if os.path.isfile(comment_log):
	for line in open(comment_log, 'r'):
		ids.append(line.strip())

r = praw.Reddit(user_agent='stl v0.1')
r.login(user, password)

for com in r.get_subreddit(subreddit).get_comments(limit=comment_limit):
	if com.id in ids:
		continue
	com.upvote()
	print "***Upvoted:\n %s\n\n" % (com.body)
	ids.append(com.id)

for post in r.get_subreddit(subreddit).get_new(limit=sub_limit):
	if post.id in ids:
		continue
	post.upvote()
	ids.append(post.id)

f = open(comment_log, 'w')
for i in ids:
	f.write("%s\n" % (i))
