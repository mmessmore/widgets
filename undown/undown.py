#!/usr/bin/env python

# Fight back against a downvoting bot, by responsibly giving 1 upvote
# to recent comments in a subreddit

import praw
import os
import sys

comment_limit = 200

# This just keeps me from hardcoding creds
# Needs a file in ~/.undown with format
# user
# password
# subreddit you want to patrol (no /r/)
settings = open(os.path.join(os.environ['HOME'], '.undown'))
user = settings.readline().strip()
password = settings.readline().strip()
subreddit = settings.readline().strip()

# Note this should probably be purged after some length of time
comment_log = os.path.join(os.environ['HOME'], '.undown.log')

ids = []
if os.path.isfile(comment_log):
	for line in open(comment_log, 'r'):
		ids.append(line.strip())

r = praw.Reddit(user_agent='undown v0.1')
r.login(user, password)

for com in r.get_subreddit(subreddit).get_comments(limit=comment_limit):
	if com.id in ids:
		continue
	com.upvote()
	print "***Upvoted:\n %s\n\n" % (com.body)
	ids.append(com.id)

f = open(comment_log, 'w')
for i in ids:
	f.write("%s\n" % (i))
